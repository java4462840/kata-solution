package com.areolla.testdevenv;

import java.util.TreeMap;

public class RomanNumber {

    private final TreeMap<Integer, String> romanNumerals = new TreeMap<>();

    public RomanNumber() {
        romanNumerals.put(1, "I");
        romanNumerals.put(4, "IV");
        romanNumerals.put(5, "V");
        romanNumerals.put(9, "IX");
        romanNumerals.put(10, "X");
        romanNumerals.put(40, "XL");
        romanNumerals.put(50, "L");
        romanNumerals.put(90, "XC");
        romanNumerals.put(100, "C");
        romanNumerals.put(400, "CD");
        romanNumerals.put(500, "D");
        romanNumerals.put(900, "CM");
        romanNumerals.put(1000, "M");
    }

    public String convert(int n) {
        StringBuilder result = new StringBuilder();
        for (int decimalValue : romanNumerals.descendingKeySet()) {
            while (n >= decimalValue) {
                result.append(romanNumerals.get(decimalValue));
                n -= decimalValue;
            }
        }
        return result.toString();
    }
}
